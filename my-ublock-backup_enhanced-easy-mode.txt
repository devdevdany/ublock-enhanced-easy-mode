{
  "timeStamp": 1704056279584,
  "version": "1.54.0",
  "userSettings": {
    "advancedUserEnabled": true,
    "externalLists": "https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt",
    "importedLists": [
      "https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt"
    ]
  },
  "selectedFilterLists": [
    "user-filters",
    "ublock-filters",
    "ublock-badware",
    "ublock-privacy",
    "ublock-quick-fixes",
    "ublock-unbreak",
    "easylist",
    "adguard-spyware-url",
    "easyprivacy",
    "urlhaus-1",
    "plowe-0",
    "https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt"
  ],
  "hiddenSettings": {},
  "whitelist": [
    "about-scheme",
    "chrome-extension-scheme",
    "chrome-scheme",
    "edge-scheme",
    "moz-extension-scheme",
    "opera-scheme",
    "vivaldi-scheme",
    "wyciwyg-scheme"
  ],
  "dynamicFilteringString": "behind-the-scene * * noop\nbehind-the-scene * inline-script noop\nbehind-the-scene * 1p-script noop\nbehind-the-scene * 3p-script noop\nbehind-the-scene * 3p-frame noop\nbehind-the-scene * image noop\nbehind-the-scene * 3p noop\n* facebook.com * block\n* facebook.net * block\n* fbcdn.net * block\nfacebook.com facebook.com * noop\nfacebook.com facebook.net * noop\nfacebook.com fbcdn.net * noop\n* google.com * block\ngoogle.com google.com * noop\n* instagram.com * block\ninstagram.com instagram.com * noop\n* youtube.com * block\nyoutube.com youtube.com * noop\n* tiktok.com * block\ntiktok.com tiktok.com * noop\n* twitter.com * block\n* x.com * block\ntwitter.com twitter.com * noop\nx.com x.com * noop\n* * 3p-frame block\nyoutube.com google.com * noop\n* google.net * block\n* microsoft.com * block\nmicrosoft.com microsoft.com * noop\n* amazon.com * block\namazon.com amazon.com * noop\n* apple.com * block\napple.com apple.com * noop\n* googletagservices.com * block\n* taboola.com * block",
  "urlFilteringString": "",
  "hostnameSwitchesString": "no-large-media: behind-the-scene false\nno-csp-reports: * true",
  "userFilters": ""
}